import {StaticPack} from './build/static-pack';

StaticPack
    .setProjectInfo({
        author: {
            name: 'Drosalys',
            url: 'https://drosalys.fr',
        },
    })
    .addStyleEntry('styles', './styles/index.scss')
    .setFavicon('./assets/images/favicon.png')
    .addEntry('scripts', './scripts/index.ts')
;

export default StaticPack.getWebpackConfig();
