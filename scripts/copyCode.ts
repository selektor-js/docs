const codeWrappers = document.querySelectorAll('.code-wrapper');
codeWrappers.forEach(codeWrapper => copyCode(codeWrapper));

function copyCode(codeWrapper) {
    const btnCopy = codeWrapper.querySelector('.copy-button');
    btnCopy.addEventListener('click', () => {
        const text = codeWrapper.querySelector('.code-showcase').innerText;
        let elementToCopy = document.createElement('textarea');
        document.body.appendChild(elementToCopy);

        elementToCopy.value = text;
        elementToCopy.select();
        document.execCommand('copy');
        document.body.removeChild(elementToCopy);
    });
}
