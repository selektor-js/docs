import { Selektor } from '@selektor/selektor';

// If you want to init existing selektor by data attribute.
Selektor.scanHTMLSection();

try {
    const selects = document.querySelectorAll("select");
    selects.forEach(select => new Selektor(select));
} catch (e) {
    console.log('Select not found !');
}