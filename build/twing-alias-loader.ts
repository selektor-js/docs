import {TwingLoaderInterface} from 'twing/dist/types/lib/loader-interface';
import {TwingSource} from 'twing';
import {LoaderContext} from 'webpack';
import slash from 'slash';
import {stat as fsStat, readFile, Stats} from 'fs';
import {dirname} from 'path';

export class TwingAliasLoader implements TwingLoaderInterface {
    protected cache: Map<string, string> = new Map();

    constructor(
        private webpackLoader: LoaderContext<any>,
    ) {}

    exists(name: string, from: TwingSource): Promise<boolean> {
        return this.findTemplate(name, from, false)
            .then(() => {
                return true;
            })
            .catch(() => {
                return false;
            })
        ;
    }

    getCacheKey(name: string, from: TwingSource): Promise<string> {
        return this.findTemplate(name, from, true);
    }

    getSourceContext(name: string, from: TwingSource): Promise<TwingSource> {
        return this.findTemplate(name, from, true)
            .then((path: string) => {
                return new Promise((resolve) => {
                    readFile(path, 'UTF-8', (err, data: string) => {
                        resolve(new TwingSource(data, name, path));
                    });
                });
            })
        ;
    }

    isFresh(name: string, time: number, from: TwingSource): Promise<boolean> {
        return this.findTemplate(name, from, true)
            .then((name: string) => {
                return new Promise((resolve) => {
                    fsStat(name, (err, stat: Stats) => {
                        resolve(stat.mtime.getTime() < time);
                    });
                });
            })
        ;
    }

    resolve(name: string, from: TwingSource, shouldThrow?: boolean): Promise<string> {
        return this.findTemplate(name, from, shouldThrow);
    }

    private findTemplate(name: string, from: TwingSource, shouldThrow: boolean = true): Promise<string> {
        if (this.cache.has(name)) {
            return Promise.resolve(this.cache.get(name));
        }

        return new Promise((resolve, reject) => {
            this.webpackLoader.resolve(dirname(from.getName()), name, (err: Error|null, result?: string) => {
                if (err) {
                    if (shouldThrow) {
                        throw err;
                    }

                    reject(err);
                } else {
                    const path = slash(result);
                    this.cache.set(name, path);
                    resolve(path);
                }
            });
        });
    }
}
