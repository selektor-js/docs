import Encore from '@symfony/webpack-encore';
import HtmlWebpackPlugin from 'html-webpack-plugin';
import {default as SitemapWebpackPlugin} from 'sitemap-webpack-plugin';
import RobotstxtPlugin from 'robotstxt-webpack-plugin';
import FaviconsWebpackPlugin from 'favicons-webpack-plugin';
import {URL} from 'url';
import {sync} from 'glob';
import {statSync} from 'fs';
import {resolve} from 'path';
import AssetsWebpackPlugin from 'assets-webpack-plugin';
import {WebpackManifestPlugin} from '@symfony/webpack-encore/lib/webpack-manifest-plugin/index';

interface Page {
    viewFile: string;
    uriPath: string;
    realPath: string;
}

interface ProjectInfo {
    name?: string;
    description?: string;
    version?: string;
    author?: {
        name: string;
        url: string;
    };
}

class StaticPackInternal {
    private baseUrl: URL = new URL('https://localhost:8000');
    private language: string = 'fr-FR';
    private favicon: false|string = false;
    private info: ProjectInfo = {};
    private globalVariables: { [key: string]: any } = {};
    private pagesDirectory: string;

    constructor() {
        this.addAliases({
            templates: resolve('.', 'templates/'),
            pages: resolve('.', 'pages/'),
            assets: resolve('.', 'assets/'),
        });
    }

    public setProjectInfo(info: ProjectInfo): StaticPackInternal {
        this.info = info;
        return this;
    }

    public addGlobals(variables: { [key: string]: any }): StaticPackInternal {
        Object.assign(this.globalVariables, variables);
        return this;
    }

    public addGlobal(name: string, value: any): StaticPackInternal {
        this.globalVariables[name] = value;
        return this;
    }

    public setBaseUrl(baseUrl: string): StaticPackInternal {
        this.baseUrl = new URL(baseUrl.split(/[?#]/)[0]);
        this.baseUrl.hash = null;
        this.baseUrl.search = null;
        return this;
    }

    public setFavicon(favicon: string): StaticPackInternal {
        this.favicon = favicon;
        return this;
    }

    public addStyleEntry(name: string, src: string): StaticPackInternal {
        Encore.addStyleEntry(name, src);
        return this;
    }

    public addEntry(name: string, src: string): StaticPackInternal {
        Encore.addEntry(name, src);
        return this;
    }

    public addAlias(alias: string, target: string): StaticPackInternal {
        if (!alias.match(/^[a-zA-Z][a-zA-Z0-9_]*$/)) {
            throw new Error(`Alias must start by a letter and contain only letters, numbers or underscores. Given "${alias}" is wrong.`);
        }

        if ('pages' === alias) {
            this.pagesDirectory = target;
        }

        Encore.addAliases({
            ['@'+alias]: target,
        });

        return this;
    }

    public addAliases(aliases: {[key: string]: string}): StaticPackInternal {
        for (const [alias, target] of Object.entries(aliases)) {
            this.addAlias(alias, target);
        }

        return this;
    }

    public getWebpackConfig(): object {
        // Compatibility with previous version
        if (undefined !== process.env.DEPLOY_HOST) {
            console.warn('Usage of DEPLOY_HOST env is deprecated. Use DEPLOY_BASE_URL instead.');
            process.env.DEPLOY_BASE_URL = process.env.DEPLOY_HOST;
        }

        // Force override of base url if env set.
        if (undefined !== process.env.DEPLOY_BASE_URL) {
            this.setBaseUrl(process.env.DEPLOY_BASE_URL);
        }

        if (!Encore.isRuntimeEnvironmentConfigured()) {
            Encore.configureRuntimeEnvironment(process.env.NODE_ENV || 'dev');
        }

        if (Encore.isProduction() && ('localhost' === this.baseUrl.hostname || '127.0.0.1' === this.baseUrl.hostname || '[::]' === this.baseUrl.hostname)) {
            console.warn('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!');
            console.warn('!!! Building production. Use "DEPLOY_BASE_URL" env or "setBaseUrl" method to set your base URL !!!');
            console.warn('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!');
            console.warn('');
        }

        this.checkProjectInfo();

        if (Encore.isDevServer()) {
            Encore.configureDevServerOptions((options) => {
                options.port = this.baseUrl.port;
                options.host = this.baseUrl.hostname;
                options.https = null !== this.baseUrl.protocol.match(/https/);
                options.historyApiFallback = {
                    rewrites: [
                        { from: /./, to: '/404.html' },
                    ],
                };
            });
            Encore.setPublicPath(this.baseUrl.pathname);
        } else {
            Encore.setPublicPath(this.baseUrl.href);
        }

        Encore
            .setOutputPath('./dist')
            .setManifestKeyPrefix('')
            .enableSingleRuntimeChunk()
            .cleanupOutputBeforeBuild()
            .enableBuildNotifications()
            .enableSourceMaps(!Encore.isProduction())
            .enableVersioning(Encore.isProduction())

            .configureBabel((config) => {
                config.plugins.push('@babel/plugin-proposal-class-properties');
            })
            .configureBabelPresetEnv((config) => {
                config.useBuiltIns = 'usage';
                config.corejs = 3;
            })

            .enableSassLoader()
            .enableTypeScriptLoader()
            //.enableIntegrityHashes(Encore.isProduction())
        ;

        this.registerTwig();
        this.registerPages();
        this.registerSitemap();
        this.registerRobotsTxt();
        this.registerFavicons();

        const config = Encore.getWebpackConfig();

        config.experiments = {
            executeModule: true,
        };

        // Remove not used plugins.
        config.plugins = config.plugins.filter((plugin) => {
            return !(
                plugin instanceof AssetsWebpackPlugin
                || plugin instanceof WebpackManifestPlugin
            );
        });

        // Fix output path.
        config.output.publicPath = this.baseUrl.toString();

        return config;
    }

    private registerTwig(): void {
        this.addGlobals({
            base_url: this.baseUrl,
            language: this.language,
        });

        Encore
            .addLoader({
                test: /\.twig$/,
                use: {
                    loader: require.resolve('./webpack-loader.ts'),
                    options: {
                        variables: this.globalVariables,
                        base_url: this.baseUrl,
                    },
                },
            })
        ;
    }

    private registerPages(): void {
        for (const page of this.pages) {
            Encore
                .addPlugin(new HtmlWebpackPlugin({
                    template: page.realPath,
                    filename: page.uriPath,
                    minify: {
                        collapseWhitespace: Encore.isProduction(),
                        removeComments: Encore.isProduction(),
                        removeRedundantAttributes: Encore.isProduction(),
                        removeScriptTypeAttributes: Encore.isProduction(),
                        removeStyleLinkTypeAttributes: Encore.isProduction(),
                        useShortDoctype: true,
                    },
                }))
            ;
        }
    }

    private registerSitemap(): void {
        Encore
            .addPlugin(new SitemapWebpackPlugin({
                base: this.baseUrl.href,
                paths: this.pages.filter((page) => {
                    return '404.html' !== page.uriPath;
                }).map((page) => {
                    return {
                        path: page.uriPath.replace(/index\.html$/, ''),
                        lastmod: statSync(page.realPath).mtime.toISOString(),
                    };
                }),
                options: {
                    skipGzip: true,
                    priority: 0.9,
                    changefreq: 'monthly',
                } as any,
            }))
        ;
    }

    private registerRobotsTxt(): void {
        Encore
            .addPlugin(new RobotstxtPlugin({
                sitemap: this.baseUrl.href.replace(/\/$/, '') + '/sitemap.xml.gz',
                host: this.baseUrl.host,
            }))
        ;
    }

    private registerFavicons(): void {
        if (this.favicon) {
            Encore.addPlugin(new FaviconsWebpackPlugin({
                logo: this.favicon,
                mode: 'webapp',
                devMode: 'webapp',
                favicons: {
                    lang: this.language,
                    appName: this.info.name || null,
                    appDescription: this.info.description || null,
                    developerName: this.info.author.name || null,
                    developerURL: this.info.author.url || null,
                    version: this.info.version || null,
                    icons: {
                        android: true,
                        appleIcon: true,
                        appleStartup: false,
                        coast: false,
                        favicons: true,
                        firefox: true,
                        windows: true,
                        yandex: true,
                    },
                },
            }));
        }
    }

    private checkProjectInfo(): void {
        console.info('### Checking Project Info ###');

        if (!this.info.name) {
            console.warn('Missing project name');
        }

        if (!this.info.description) {
            console.warn('Missing project description');
        }

        if (!this.info.version) {
            console.warn('Missing project version');
        }

        if (!this.info.author) {
            console.warn('Missing project author');
        }
    }

    private get pages(): Array<Page> {
        return sync('**/*.html?(.twig)', {
            cwd: this.pagesDirectory,
        }).map((viewFile: string) => {
            return {
                viewFile: '@pages/'+viewFile,
                uriPath: viewFile.replace(/\.twig$/, ''),
                realPath: resolve(this.pagesDirectory, viewFile),
            } as Page;
        });
    }
}

export const StaticPack = new StaticPackInternal();
