import {TwingEnvironment, TwingLoaderRelativeFilesystem, TwingTemplate, TwingLoaderChain} from 'twing';
import slash from 'slash';
import {TwingAliasLoader} from './twing-alias-loader';
import { getOptions } from 'loader-utils';
import { validate } from 'schema-utils';
import {Schema} from 'schema-utils/declarations/validate';
import {StaticExtension} from './twing/static-extension';

const schema: Schema = {
    type: 'object',
    properties: {
        variables: {
            type: 'object',
        },
        base_url: {
            type: 'object',
        },
    },
    additionalProperties: false,
};

export default function (source) {
    // Load options.
    const options = getOptions(this);

    validate(schema, options, {
        name: 'Twing Loader',
        baseDataPath: 'options',
    });

    const loader = new TwingLoaderChain([
        new TwingLoaderRelativeFilesystem(),
        new TwingAliasLoader(this),
    ]);

    const environment = new TwingEnvironment(loader, {
        debug: 'development' === this.mode,
        source_map: this.sourceMap,
        strict_variables: true,
    });

    environment.addExtension(new StaticExtension(this, options.base_url), 'StaticExtension');

    if (options.variables) {
        for (const [name, value] of Object.entries(options.variables)) {
            environment.addGlobal(name, value);
        }
    }

    const relativePath = slash(this.resource)
        .replace(slash(this.rootContext), '')
        .replace(/^\//, '')
    ;
    const callback = this.async();

    environment.load(relativePath)
        .then((template: TwingTemplate) => {
            return template.render({});
        })
        .then((output: string) => {
            callback(null, `export default ${JSON.stringify(output)}`);
        })
        .catch((e) => {
            return callback(e);
        })
    ;
}
