import {TwingError, TwingExtension, TwingFunction, TwingTemplate} from 'twing';
import {LoaderContext} from 'webpack';
import {URL} from 'url';

export class StaticExtension extends TwingExtension {
    constructor(
        private webpackLoader: LoaderContext<any>,
        private baseUrl: URL,
    ) {
        super();
    }

    getFunctions(): Array<TwingFunction> {
        return [
            new TwingFunction('asset', (path: string) => {
                return this.webpackLoader.importModule(`@assets/${path}`);
            }, [
                {name: 'path'},
            ], {
                is_safe: ['all'],
            }),
            new TwingFunction('path', (template: TwingTemplate, name: string) => {
                return new Promise((resolve, reject) => {
                    this.webpackLoader.resolve(template.templateName, `@pages/${name}.html.twig`, (err) => {
                        if (null === err) {
                            const baseUrl = this.baseUrl.toString().replace(/\/$/, '');
                            const shortName = name.replace(/^\//, '')
                                .replace(/\.twig$/, '')
                                .replace(/\.html$/, '')
                            ;

                            if (shortName.match(/(\/|^)index$/)) {
                                resolve(`${baseUrl}/${shortName.replace(/(\/|^)index$/, '')}`);
                            } else {
                                resolve(`${baseUrl}/${shortName}.html`);
                            }
                        } else {
                            reject(new TwingError('Cannot use path function.', null, template.source, err));
                        }
                    });
                });
            }, [
                {name: 'template'},
                {name: 'name'},
            ], {
                needs_template: true,
                is_safe: ['all'],
            }),
        ];
    }
}
