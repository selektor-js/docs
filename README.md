Static starter
==============

Static starter project used at Drosalys company. Perfect for landing/one static page.

## Requirements

- NodeJS 14

## Install

Replace `project_name` by the name of the projet you when to create.

```bash
git clone git@gitlab.com:drosalys/starters/static.git project_name
rm -Rf ./.git
yarn
```

After that, you can add the code to your GIT repository.

## Available commands

### Development mode

Start a dev web server, listening by default at https://localhost:8000. You can customize listing port. By default, the
server support SSL with auto-signed certificate, you need to accept the exception in your browser.

```bash
yarn serve
```

### Prod build

Build the static app in the `dist` directory.

```bash
yarn build
```

### Use linter

Linter are used to check some errors or invalid source code. There are two linter in this repository, one for SCSS and
another for TS:

```bash
yarn tslint
yarn scsslint
```

Those two commands will give you info were the code is not valid and why.

## Information

### Styles

Stylesheets are written in [SCSS](https://sass-lang.com/). You can find some default scss files in `/styles`. You do
not need to add any stylesheet link to your html files, Webpack will add it automatically for you at the right place.

When you need to refer to an image in you SCSS, put the image file into the `/assets/images` directory and make the path
inside SCSS file absolute to the project root. eg: `background-image: url('/assets/images/my-img.jpg')`.

### Scripts

Scripts are written in [Typescript](https://www.typescriptlang.org/). You can find some default scss files in
`/scripts`. You do not need to add any stylesheet link to your html files, Webpack will add it automatically for you
at the right place.

### Favicon

The favicon is handle by Webpack. You need to edit the line `.setFavicon('./assets/images/favicon.png')` from
`webpack.config.ts` to set to your favicon path.

### HTML files

HTML file are using [Twing.js](https://nightlycommit.github.io/twing/), it use syntax of Symfony's template engine
[Twig](https://twig.symfony.com/).

If you add a new html file to sources, you have to end its name by `.twig` and restart your dev server.

You can use `asset` Twig's function for assets linking. For example, including an image `assets/images/favicon.png`,
use `<img src="{{ asset('images/favicon.png') }} alt="An alt" title="title">`.

You can use `path` Twig's function to make hypertext link between pages. Examples
- Go back to home page: `<a href="{{ path('index') }}">Home</a>`
- Go to the template page `/pages/example/foo.html.twig`: `<a href="{{ path('example/foo') }}">Link</a>`

### 404 error page.

Do not forget to implement the 404 template locate in `pages/404.html.twig`.

## Maintainers

- The [Drosalys](https://www.drosalys-web.fr/) company
- [Benjamin Georgeault](https://gitlab.com/WedgeSama)

## LICENSE

The code used to generate this stack is release under MIT licence.

## TODO

- Auto handle multiple html files 
